import React, { useState, useContext } from "react";
import { Text, StyleSheet, View, TextInput, Button } from "react-native";
import { Context as AlbumContext } from "../context/AlbumContext";
// import { Context as AuthContext } from "../context/AuthContext";
import axios from "../api/albums";

const AlbumCreationScreen = ({ navigation }) => {
  const { createNewAlbum } = useContext(AlbumContext);
  // const AuthContextUse = useContext(AuthContext);
  const [albumName, setAlbumName] = useState("");
  const [description, setDescription] = useState("");

  const userID = "602ea663a78fd473a871fe72";
  return (
    <View>
      <Text style={styles.text}>AlbumCreationScreen!</Text>
      {/* <Button
        onPress={() => navigation.navigate('Components')}
        title="Go to Components Demo"
      /> */}

      <Text style={styles.label}>Enter Album Name:</Text>
      <TextInput
        style={styles.InputStyle}
        value={albumName}
        onChangeText={(text) => setAlbumName(text)}
      />
      <Text style={styles.label}>Enter Description:</Text>
      <TextInput
        multiline={true}
        textAlignVertical={"top"}
        numberOfLines={10}
        style={styles.InputStyle}
        value={description}
        onChangeText={(text) => setDescription(text)}
      />
      <Button
        title="Create New Album"
        onPress={() => {
          createNewAlbum( userID,albumName, description, () => {
            // console.log(AuthContextUse.userID);
            navigation.navigate("Albums");
          });
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  text: {
    fontSize: 30,
  },
  InputStyle: {
    fontSize: 18,
    borderWidth: 1,
    borderColor: "black",
    marginBottom: 15,
    padding: 5,
    margin: 5,
  },
  label: {
    fontSize: 20,
    marginBottom: 5,
  },
});

export default AlbumCreationScreen;
