import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import { Camera } from 'expo-camera';
import { MaterialIcons, Ionicons } from '@expo/vector-icons';

const CameraScreen = () => {
  const [hasPermission, setHasPermission] = useState(null);
  const [cameraType, setCameraType] = useState('back');
  const [flashMode, setFlashMode] = useState('off');
  const [flashIcon, setFlashIcon] = useState('flash-off');

  const cameraRef = React.createRef(); // create ref for Camera component
  useEffect(() => {
    // ask for permission first time the screen is focused
    (async () => {
      const { status } = await Camera.requestPermissionsAsync();
      setHasPermission(status === 'granted');
    })();
  }, []);

  if (hasPermission === null) {
    // check permission
    return <View />;
  }
  if (hasPermission === false) {
    // check permission
    return <Text>No access to camera</Text>;
  }

  const takePicture = async () => {
    // function for taking a picture , onPress camera button
    try {
      if (cameraRef.current) {
        const options = { quality: 0.5, base64: true };
        const photo = await cameraRef.current.takePictureAsync(); // async function wiil return photo  elemnt with(uri.width,height)
        console.log(photo);
      }
    } catch (err) {
      console.log(err);
    }
  };

  const handleFlashMode = () => {
    if (flashMode === 'on') {
      setFlashMode('auto');
      setFlashIcon('flash-auto');
    } else if (flashMode === 'off') {
      setFlashMode('on');
      setFlashIcon('flash-on');
    } else {
      setFlashMode('off');
      setFlashIcon('flash-off');
    }
  };

  const switchCamera = () => {
    if (cameraType === 'back') {
      setCameraType('front');
    } else {
      setCameraType('back');
    }
  };

  return (
    <View style={styles.container}>
      <Camera
        style={styles.camera}
        ref={cameraRef}
        type={cameraType}
        flashMode={flashMode}
      >
        <View style={styles.buttonContainer}>
          <TouchableOpacity // touch button for fliping camera
            style={styles.button}
            onPress={switchCamera}
          >
            <Ionicons name="camera-reverse-sharp" style={styles.button} />
          </TouchableOpacity>

          <TouchableOpacity // touch button for turn on/off flash
            style={styles.button}
            onPress={handleFlashMode}
          >
            <MaterialIcons name={flashIcon} style={styles.button} />
          </TouchableOpacity>
        </View>
        {/* //touch button for taking a picture */}
        <TouchableOpacity style={{ alignSelf: 'center' }} onPress={takePicture}>
          <View // camera ring
            style={styles.cameraBtnRadius}
          >
            <View // while circle to fill the camera button
              style={styles.cameraBtn}
            ></View>
          </View>
        </TouchableOpacity>
      </Camera>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  camera: {
    flex: 1,
  },
  buttonContainer: {
    flex: 1,
    flexDirection: 'row',
    // justifyContent: 'flex-end',
    margin: 20,
  },
  button: {
    flex: 0.1,
    alignItems: 'center',
    fontSize: 24,
    alignSelf: 'flex-end',

    color: 'white',
  },
  text: {
    fontSize: 18,
    color: 'white',
  },
  cameraBtn: {
    borderWidth: 2,
    borderRadius: 50,
    borderColor: 'white',
    height: 40,
    width: 40,
    backgroundColor: 'white',
  },
  cameraBtnRadius: {
    borderWidth: 2,
    borderRadius: 50,
    borderColor: 'white',
    height: 50,
    width: 50,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default CameraScreen;
