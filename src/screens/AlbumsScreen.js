import React,{useContext,useEffect} from 'react';
import { Text, StyleSheet, View,TouchableOpacity ,FlatList } from 'react-native';
import {Context} from '../context/AlbumContext';
import { Feather,AntDesign} from '@expo/vector-icons';
import ResultsDetailAlbum from '../components/ResultsDetailAlbum'


const AlbumsScreen = ({ navigation  }) => {

  const {state,getAlbums,deleteAlbum} = useContext(Context);

  useEffect(()=>{
    getAlbums();

    const listener = navigation.addListener('didFocus',()=>{
      getAlbums();
    });

    return ()=>{
      listener.remove();
    }; //invoked only if instance of AlbumsScreen removed -- sooon as our component isnt visible so clean up the dash
  },[]);


  return (
    <View style={{flex: 1}}>
      <Text style={styles.text}>My Albums</Text>
      
      <View>

            <FlatList
            data={state}
            keyExtractor={(album)=>album.id.toString()}
            renderItem={({item})=>{
                return (
                  <View>
                    <TouchableOpacity onPress={()=>navigation.navigate('SingleAlbum',{id: item.id})}>
                        <View style={styles.row}>
                            <Text style={styles.name}>{item.name} - {item.id}</Text>

                            {/* //Trash icon */}
                            <TouchableOpacity onPress={
                              ()=>deleteAlbum(item.id,()=>{
                                navigation.navigate('Albums');
                                })
                            }>
                                <Feather name="trash" size={24} color="black" />
                            </TouchableOpacity>

                        </View>
                    </TouchableOpacity>

                    {/* //////AlbumImages - FlatList////// */}
                    <FlatList
                      showsHorizontalScrollIndicator={false}
                      horizontal
                      data={item.photos}
                      keyExtractor={(photo)=>photo.id}
                      renderItem={({item})=>{
                          return(<TouchableOpacity 
                                  onPress={()=>console.log("pressed on selected image")}
                                  >
                              <ResultsDetailAlbum result={item}/>
                              </TouchableOpacity>
                          )
                      }}
                    />
                  </View>

                );
            }}
            />

      </View>
    

    <View style={styles.plusButton}>
      <TouchableOpacity
      onPress={()=>navigation.navigate('AlbumCreation')}
      >
      <AntDesign name="pluscircleo" size={64} color="black" />
      </TouchableOpacity>
    </View>      
      </View>

  );
};


//-- Side Buttons --
// AlbumsScreen.navigationOptions = ({navigation})=>{
//   return{
//       headerRight: ()=> <TouchableOpacity onPress={()=>
//                           navigation.navigate('AlbumCreation')
//                       }>
//                           {/* <Entypo style={{marginRight:10}} name="plus" size={24} color="black" /> */}
//                           <Text>Check</Text>
//                   </TouchableOpacity>
//   };
// };

const styles = StyleSheet.create({
  text: {
    fontSize: 30,
    marginBottom:5,
  },
  row:{
    flexDirection:'row',
    justifyContent:'space-between',
    paddingVertical:20,
    borderBottomWidth:1,
    borderColor:'grey',

},
name:{
    fontSize:18,
},
plusButton: {
  position: 'absolute',
  bottom:10,
  right:10,
  marginBottom:10,
  marginRight:10,
},
});

export default AlbumsScreen;