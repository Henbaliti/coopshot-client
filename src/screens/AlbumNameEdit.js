import React,{useState,useContext} from 'react';
import { Text, StyleSheet, View, Button,TextInput} from 'react-native';
import {Context} from '../context/AlbumContext'

const AlbumNameEdit = ({ navigation }) => {

  const {state,editAlbum} = useContext(Context);
  const album = state.find((album)=>album.id===navigation.getParam('id'));
  
  const [albumName,setAlbumName] = useState(album.name);

  return (
    <View>
      <Text style={styles.label}>Enter New Album Name:</Text>
      <TextInput style={styles.InputStyle} value={albumName} onChangeText={(text)=>setAlbumName(text)}/>

      <Button 
      title="Save Changes" 
      onPress={()=>{editAlbum(navigation.getParam('id'),albumName,album.description,()=>navigation.navigate('SingleAlbum',{id: album.id}))}
            
      }/>


    </View>
  );
};

const styles = StyleSheet.create({
    text: {
        fontSize: 30
      },
      InputStyle:{
        fontSize:18,
        borderWidth:1,
        borderColor:'black',
        marginBottom:15,
        padding:5,
        margin:5,
    },
    label:{
        fontSize:20,
        marginBottom:5,
    },
});

export default AlbumNameEdit;