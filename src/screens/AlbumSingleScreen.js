import React,{useContext,useState,useEffect} from 'react';
import { Text, StyleSheet, View,TouchableOpacity,Image,Platform } from 'react-native';
import * as ImagePicker from 'expo-image-picker';
import {Context} from '../context/AlbumContext'
import { Feather ,Ionicons,MaterialIcons} from '@expo/vector-icons';
import { FlatList } from 'react-native-gesture-handler';
import { FAB, Portal, Provider } from 'react-native-paper';



const imagesArr = ['https://images.all-free-download.com/images/graphicthumb/beautiful_scenery_04_hd_pictures_166258.jpg'
,'https://images.all-free-download.com/images/graphicthumb/beautiful_scenery_04_hd_pictures_166258.jpg',
'https://images.all-free-download.com/images/graphicthumb/beautiful_scenery_04_hd_pictures_166258.jpg',
'https://images.all-free-download.com/images/graphicthumb/beautiful_scenery_04_hd_pictures_166258.jpg'];



const AlbumSingleScreen = ({ navigation}) => {


  //////////---------------******Image Picker******------------///////////////////
  
  const [image, setImage] = useState(null);

  useEffect(() => {
    (async () => {
      if (Platform.OS !== 'web') {
        const { status } = await ImagePicker.requestMediaLibraryPermissionsAsync();
        if (status !== 'granted') {
          alert('Sorry, we need camera roll permissions to make this work!');
        }
      }
    })();
  }, []);

  const pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
    });

    console.log(result);

    if (!result.cancelled) {
      setImage(result.uri);
    }
  };
  //////////---------------******Image Picker******------------///////////////////

  //------------------------------------------------------------------------------------------------------------
  //------------------------------------------------------------------------------------------------------------

  //////////---------------******Floating Button******------------///////////////////
  const [stateFloat, setStateFloat] = useState({ open: false });

  const onStateChange = ({ open }) => setStateFloat({ open });

  const { open } = stateFloat;
  //////////---------------******Floating Button******------------///////////////////

  //------------------------------------------------------------------------------------------------------------
  //------------------------------------------------------------------------------------------------------------

  //////////---------------******AlbumSingle - Context******------------///////////////////

  const{state} = useContext(Context);

  const album = state.find((album)=>album.id===navigation.getParam('id'));
  //////////---------------******AlbumSingle - Context******------------///////////////////


  return (
    <View style={{marginHorizontal:8,marginTop:3,flex:1}}>


      <View style={{alignItems:'flex-start',flexDirection:'row',justifyContent:'space-between'}}>
        <Text style={styles.nameStyle}>{album.name}</Text>

        {/* //Edit Icon - AlbumName*/}
        <View style={{paddingRight:240}}>
        <TouchableOpacity onPress={()=>
          navigation.navigate('EditAlbumName',{id : album.id})
        }>
            <Feather name="edit" size={18} color="black" />
        </TouchableOpacity>
        </View>
      </View>


      <View style={{alignItems:'flex-start',flexDirection:'row',justifyContent:'space-between'}}>
          <Text style={styles.descriptionStyle}>{album.description}</Text>


          {/* //Edit Icon - DescAlbum*/}
          <View>
            <TouchableOpacity onPress={()=>
            navigation.navigate('EditAlbumDesc',{id : album.id})
          }>
              <Feather name="edit" size={18} color="black" />
          </TouchableOpacity>
          </View>
      </View>

      <View>
        <FlatList
          numColumns={3}
          horizontal={false}
          data={imagesArr}
          keyExtractor={(imageUrlSt)=>imageUrlSt}
          renderItem={({item})=>(
            <View style={styles.containerImage}>
              <Image
              style={styles.image}
                source={{uri: item}}
              />
            </View>
          )}
        />
      </View>

      {/* //////////---------------******Floating Button******------------/////////////////// */}
          <Provider style={styles.providerStyle}>
            <Portal>
              <FAB.Group
              color="black"
              open={open}
              icon={open ? 'minus' : 'plus'}
              actions={[
                { icon: (props)=><Feather {...props} name="plus" size={25} color="black" />, onPress: () => console.log('Pressed More') },
                {
                  icon: (props)=><Ionicons {...props} name="ios-file-tray-stacked-sharp" size={24} color="black" />,
                  label: 'My Files',
                  onPress: () => console.log('Pressed Files'),
                },
                {
                  
                  icon: ()=><View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      
                                    <MaterialIcons name="photo-library" size={24} color="black"/>
                
                                    {image && <Image source={{ uri: image }} style={{ width: 200, height: 200 }} />}
                                  </View>
                  
                  ,
                  label: 'Gallery',
                  onPress: () => {
                    console.log('Pressed Gallery');
                    pickImage();
                  },
                },
                {
                  icon: (props)=><Ionicons {...props} name="camera" size={24} color="black" />,
                  label: 'Camera',
                  onPress: () => {
                    console.log('Pressed Camera');
                    navigation.navigate('Camera')//Need to pass here the album id
                  },
                  small: false,
                },
              ]}
              onStateChange={onStateChange}
              onPress={() => {
                if (open) {
                  // do something if the speed dial is open
                }
              }}
              />
            </Portal>
          </Provider>
      {/* //////////---------------******Floating Button******------------///////////////////         */}



    </View>
  );
};

const styles = StyleSheet.create({
  nameStyle:{
    fontWeight:'bold',
    fontSize:30,
    marginVertical:5,
  },
  descriptionStyle:{
    marginBottom:14,

  },
  providerStyle:{
    position: 'absolute',
    bottom:10,
    right:10,
    marginBottom:10,
    marginRight:10,
  },
  containerImage:{
    flex: 1/3,
  },
  image:{
    flex:1,
    aspectRatio:1/1,
  },
});

export default AlbumSingleScreen;