// import { AsyncStorage } from '@react-native-community/async-storage'; // has to be done by eject - not with expo
import { AsyncStorage } from "react-native";
import createDataContext from "./createDataContext";
import albumsApi from "../api/albums";
import { navigate } from "../navigationRef";

const authReducer = (state, action) => {
  switch (action.type) {
    case "add_error":
      return { ...state, errorMessage: action.payload };
    case "signup":
      return { errorMessage: "", token: action.payload };
    case "signin":
      return {
        errorMessage: "",
        token: action.payload[0],
        userID: action.payload[1],
      };
    case "clear_error_message":
      return { ...state, errorMessage: "" };
    case "signout":
      return { token: null, errorMessage: "" };
    default:
      return state;
  }
};

const tryLocalSignin = (dispatch) => async () => {
  const token = await AsyncStorage.getItem("token");
  if (token) {
    dispatch({ type: "signin", payload: token });
    navigate("Albums");
  } else {
    navigate("SignUp");
  }
};

const signup = (dispatch) => async ({ email, password }) => {
  try {
    //Make an API request
    const response = await albumsApi.post("/signup", { email, password });
    await AsyncStorage.setItem("token", response.data.token);
    // await AsyncStorage.setItem("userID", response.data.userID);
    dispatch({ type: "signup", payload: response.data.token });

    navigate("Albums");
  } catch (err) {
    dispatch({
      type: "add_error",
      payload: "Something went wrong with sign up",
    });
  }
};

const clearErrorMessage = (dispatch) => () => {
  dispatch({ type: "clear_error_message" });
};

const signin = (dispatch) => {
  return async ({ email, password }) => {
    try {
      const response = await albumsApi.post("/signin", { email, password });
      await AsyncStorage.setItem("token", response.data.token);
      await AsyncStorage.setItem("retUserId", response.data.userID);
      console.log(response.data.userID);
      // Handle success by updating state
      dispatch({
        type: "signin",
        payload: [response.data.token, response.data.userID],
      });
      navigate("Albums");
    } catch (err) {
      // Handle failure by showing error message (somehow)
      dispatch({
        type: "add_error",
        payload: "Something went wrong with sign in",
      });
    }
  };
};

const signout = (dispatch) => {
  return async () => {
    // await AsyncStorage.removeItem("token");
    dispatch({ type: "signout" });
    navigate("loginFlow");
  };
};

export const { Provider, Context } = createDataContext(
  authReducer,
  { signin, signout, signup, clearErrorMessage, tryLocalSignin },
  { token: null, errorMessage: "" }
);
