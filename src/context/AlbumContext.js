import createDataContext from './createDataContext'
// import jsonServer from '../api/albums'
import axios from '../api/albums';
// import axios from 'axios'

const albumReducer =(state,action)=>{
    switch(action.type){
        case 'delete_album':
            return state.filter((album)=> album.id!==action.payload);
        case 'edit_album_name':
            return state.map((album)=>{
                return album.id === action.payload.id
                    ? {name:action.payload,description:album.description}
                : album;
            })
        case 'edit_album_desc':
            return state.map((album)=>{
                return album.id === action.payload.id
                    ? {name:album.name,description:action.payload}
                : album;
            })
        case 'edit_album':
            return state.map((album)=>{
                return album.id === action.payload.id
                    ? action.payload
                : album;
            })    

        case 'get_albums':
            return action.payload;
        default:
            return state;
    }
};

const getAlbums = dispatch=>{
    return async()=>{
        // const response = await jsonServer.get('/albums');

        //response.data === [{Album1},{Album2},{...}]
        dispatch({type:'get_albums',payload: response.data});
    };
};

const createNewAlbum = (dispatch)=>{
    return async (userID,albumName,description,callback)=>{
        // await jsonServer.post('/albums',{name : albumName,description : description});
        console.log("hi!!!!!")
        const response = await axios.post('/api/createalbum', {
            userID:userID,
            albumName:albumName,
            description:description
        },{
            'Content-Type': 'application/json',
            // 'Authorization': 'Bearer '+token
        })
        console.log("bye!!!!!");
        console.log(response);
        if(callback){
            console.log("created");
            callback();
        }
    };
};

const deleteAlbum = dispatch=>{
    return async (id,callback) =>{
        // await jsonServer.delete(`/albums/${id}`);

        dispatch({type: 'delete_album',payload:id});
        callback();
    };
};


const editAlbumDesc = (dispatch)=>{
    return async (id,description,callback)=>{

        // await jsonServer.put(`/albums/${id}`,{description : description});

        dispatch({type:'edit_album_desc',payload:{id: id,description:description}});
        callback();
    };
};

const editAlbumName = (dispatch)=>{
    return async (id,albumName,callback)=>{

        // await jsonServer.put(`/albums/${id}`,{name : albumName});

        dispatch({type:'edit_album_name',payload:{id: id,name: albumName}});
        callback();
    };
};
const editAlbum = (dispatch)=>{
    return async (id,albumName,description,callback)=>{

        // await jsonServer.put(`/albums/${id}`,{name : albumName,description:description});

        dispatch({type:'edit_album',payload:{id: id,name: albumName,description:description}});
        callback();
    };
};



//88888888888888888 --- First build a new function

//88888888888888888 --- Second adding a new aditional case to our reducer

export const {Context,Provider} = createDataContext(
    albumReducer,
    {createNewAlbum,getAlbums,deleteAlbum,editAlbumDesc,editAlbumName,editAlbum},
    []
    );