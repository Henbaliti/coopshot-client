import React from "react";
import { createStackNavigator } from "react-navigation-stack";
import { createAppContainer, createSwitchNavigator } from "react-navigation";
import SignUpScreen from "./src/screens/SignUpScreen";
import SignInScreen from "./src/screens/SignInScreen";
import AlbumSingleScreen from "./src/screens/AlbumSingleScreen";
import AlbumsScreen from "./src/screens/AlbumsScreen";
import AlbumCreationScreen from "./src/screens/AlbumCreationScreen";
import CameraScreen from "./src/screens/CameraScreen";
import { Provider as AuthProvider } from "./src/context/AuthContext";
import { Provider as AlbumProvider } from "./src/context/AlbumContext";
import { setNavigator } from "./src/navigationRef";
import ResolveAuthScreen from "./src/screens/ResolveAuthScreen";
import AlbumNameEdit from "./src/screens/AlbumNameEdit";
import AlbumDescEdit from "./src/screens/AlbumDescEdit";

const switchNavigator = createSwitchNavigator({
  ResolveAuthScreen,
  loginFlow: createStackNavigator({
    SignIn: SignInScreen,
    SignUp: SignUpScreen
  }),
  mainFlow: createStackNavigator(
    {
      AlbumCreation: AlbumCreationScreen,
      SingleAlbum: AlbumSingleScreen,
      Albums: AlbumsScreen,
      Camera: {
        screen: CameraScreen,
        navigationOptions: {
          headerShown: false, //this will hide the header
        },
      },
      EditAlbumName: AlbumNameEdit,
      EditAlbumDesc: AlbumDescEdit,
    },
    {
      //initialRouteName: 'AlbumCreation',
      // defaultNavigationOptions: {
      //   title: 'App',
      // },
    },
  ),
},{
  initialRouteName: "loginFlow",
});

const App = createAppContainer(switchNavigator);

export default () => {
  return (
    <AuthProvider>
      <AlbumProvider>
        <App
          ref={(navigator) => {
            setNavigator(navigator);
          }}
        />
      </AlbumProvider>
    </AuthProvider>
  );
};
